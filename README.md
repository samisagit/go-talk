# Go talk

Side car for the Go talk

## Installation

* Go to https://golang.org/dl/ and download the relevent installer
* Run `tar -C /usr/local -xzf go1.13.4.darwin-amd64.tar.gz` (replace versions/architecture where neccessary)
* Run `export PATH=$PATH:/usr/local/go/bin` (or add this to your shell rc file to make it permanent)
* Run `go version`

## Usage

#### Hello world

* Clone this repo where ever you want to have your Go code.
* Run `go run {path to repo}/cmd/helloworld/main.go` to quickly run your application
* Run `go build {path to repo}/cmd/helloworld/main.go` to build a binary from your applciation code
* Run `ls {path to repo}` to observe your shiny binary

#### Hello world server

* Clone this repo where ever you want to have your Go code.
* Run `go run {path to repo}/cmd/httpserver/main.go` to quickly run your application
* Run `go build {path to repo}/cmd/httpserver/main.go` to build a binary from your applciation code
* Run `curl localhost:8080`

#### JSON server

* Clone this repo where ever you want to have your Go code.
* Run `go run {path to repo}/cmd/jsonserver/main.go` to quickly run your application
* Run `go build {path to repo}/cmd/httpserver/main.go` to build a binary from your applciation code
* Run `curl -d '{"username": "userA", "password": "test123"}' localhost:8080`

#### Dockerize

* Clone this repo where ever you want to have your Go code.
* Run `cd {path to repo}/cmd/dockerize`
* Run `docker build . -t hellogo:0.0.0` to build your docker image
* Run `docker run hellogo:0.0.0` to run your docker image
* Run `docker inspect hellogo:0.0.0` to see all the shiny things about your docker image

## Creating your own modules

#### I want to write an application

* Create a directory for your module to live in
* Run `go mod init {some relatively unique string typically the git repo URL}`
* Create a sub directory for your main package to live in
* Create a Go file with package `main` and a func `main` in said sub directory

#### I want to write a library

* Create a directory for your module to live in
* Run `go mod init {some relatively unique string typically the git repo URL}`
* Create a Go file explicitly without package `main` or a func `main`

## Adding dependencies

#### New dependencies

* Navigate to your "module" (where the go.mod lives)
* Run `go get {git repo url of dependency}` e.g. `go get "github.com/sirupsen/logrus"`
* Run `cat {path to repo}/{path to module}/go.mod`
* Use dependency in your project e.g.
```
package main

import (
	"github.com/sirupsen/logrus"
)
  
func main() {
	logrus.WithFields(logrus.Fields{
		"animal": "walrus",
	}).Info("A walrus appears")
}
```

#### Existing dependencies

* Navigate to your "module" (where the go.mod lives)
* Run `go mod download` (similar to `npm i`)

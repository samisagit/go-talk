package superhandyservice

import (
	"fmt"
	"testing"
)

func TestAddOneToThings(t *testing.T) {
	// Go tests tend to be written in a tabular format, ie a test run across
	// many different cases
	cases := []struct {
		name           string
		input          int
		expectedOutput int
	}{
		{
			name:           "2",
			input:          2,
			expectedOutput: 3,
		},
		{
			name:           "5",
			input:          5,
			expectedOutput: 6,
		},
		{
			name:           "1",
			input:          71,
			expectedOutput: 72,
		},
		{
			name:           "0",
			input:          0,
			expectedOutput: 1,
		},
	}

	// This will loop over each case defined above and run the test against
	// these values
	for _, val := range cases {
		t.Run(fmt.Sprintf("given %s", val.name), func(t *testing.T) {
			result := AddOneToThings(val.input)

			if result != val.expectedOutput {
				t.Errorf("expected result to be %d, got %d", val.expectedOutput, result)
			}
		})
	}
}

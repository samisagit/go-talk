package main

import (
	"fmt"
)

type isRounder interface {
	isRound() bool
}

// note that we don't have to specify that we're implementing an interface, go uses
// a similar strategy to duck typing at compile time
type shape struct {
	corners int
}

// this makes the type shape satifsy the isRounder interface
func (s shape) isRound() bool {
	return s.corners == 0
}

func printRoundness(item isRounder) {
	fmt.Printf("is round: %t\n", item.isRound())
}

func main() {
	square := shape{corners: 4}

	printRoundness(square)
}

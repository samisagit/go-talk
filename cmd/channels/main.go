package main

import "fmt"

// This function takes a channel (concurrency safe queue)
func countToTen(resultChan chan int) {
	for i := 1; i < 11; i++ {
		// Write the index to the channel, this will block until it's read, unless there is a buffer
		resultChan <- i
	}

	// We're done with the channel, so close it
	close(resultChan)
}

func main() {
	// Create a channel (concurrency safe queue, in this case with no buffer)
	resultChan := make(chan int)

	// Start the go routine, which will immediately block trying to write to the channel
	go countToTen(resultChan)

	// Name our loop so we can break out of mulitple scopes
OurFancyLoop:
	// Loop until we explicitly break out
	for {
		// A select works like a switch, but reads for channels
		select {
		// Channels can return tuples of the value, and if the channel is closed
		case num, ok := <-resultChan:
			if !ok {
				// The channel is closed, break out
				break OurFancyLoop
			}

			fmt.Println(num)
		}
	}

	fmt.Println("all numbers printed")
}

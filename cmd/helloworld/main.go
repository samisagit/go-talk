package main

import (
	"fmt"
)

func main() {
	// note how the function Println starts with a captial, this means it's exported
	// and is the reason we can use it outside the fmt pacakge
	fmt.Println("hello world")
}

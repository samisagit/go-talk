package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
)

func main() {
	http.HandleFunc("/", helloServer)
	http.ListenAndServe(":8080", nil)
}

// note here we have json tags on our fields. These allow us to map json properties
// of any key to any Go struct field, and the same works in reverse below. As a side
// note, to Marshal and Unmarshal fields they MUST be exported, even if there are used
// only in this package
type request struct {
	UserName string `json:"username"`
	Password string `json:"password"`
}

type response struct {
	UserName string `json:"username"`
	Greeting string `json:"greeting"`
}

// note in this function there is the possibility that we come across errors reading the
// body, unmarshalling or marshalling the data, or writing the response. Go gives us the oppertunity
// to handle these explicity as any function can return a tuple. Idiomatic Go code will handle these
// errors as soon as they're encountered and quit early if necessary
func helloServer(w http.ResponseWriter, r *http.Request) {
	readBytes, err := ioutil.ReadAll(r.Body)
	if err != nil {
		fmt.Println(err.Error())
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	var requestBody request
	err = json.Unmarshal(readBytes, &requestBody)
	if err != nil {
		fmt.Println(err.Error())
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	responseBody := response{
		UserName: requestBody.UserName,
		Greeting: "hello",
	}

	writtenBytes, err := json.Marshal(&responseBody)
	if err != nil {
		fmt.Println(err.Error())
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	w.WriteHeader(http.StatusOK)
	w.Write(writtenBytes)
}

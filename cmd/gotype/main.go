package main

import (
	"fmt"
)

// note that this type, and it's fields are not exported (we can't use them in other packages)
type shape struct {
	corners int
}

func (s shape) isRound() bool {
	return s.corners == 0
}

func main() {
	square := shape{corners: 4}
	circle := shape{corners: 0}

	fmt.Println("square is round:", square.isRound())
	fmt.Println("circle is round:", circle.isRound())
}

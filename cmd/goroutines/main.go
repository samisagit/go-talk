package main

import (
	"fmt"
	"sync"
	"time"
)

func main() {
	// This creates a concurrency safe counter
	wg := sync.WaitGroup{}

	// We add 1 to the counter (as we're starting 1 go routine)
	wg.Add(1)
	// Note we're passing a pointer to the WaitGroup
	go countToTen(&wg)

	// We wait until the counter is set to 0
	wg.Wait()

	// lets add more
	wg.Add(3)
	for i := 0; i < 3; i++ {
		go countToTen(&wg)
	}
	wg.Wait()
}

// Note we're accepting a pointer to a WaitGroup
func countToTen(wg *sync.WaitGroup) {
	for i := 1; i < 11; i++ {
		time.Sleep(1 * time.Second)
		fmt.Println(i)
	}

	wg.Done()
}

package main

import (
	"fmt"
	"net/http"
)

func main() {
	// handleFunc takes a path and a handler function
	http.HandleFunc("/", helloServer)

	// this specifies the port to run the server on and the server to use
	// in this case we're using the default server
	http.ListenAndServe(":8080", nil)
}

// this function satisfies the constraint of a handleFunc handler
func helloServer(w http.ResponseWriter, r *http.Request) {
	fmt.Fprint(w, "hello world")
}
